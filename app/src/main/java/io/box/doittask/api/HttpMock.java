package io.box.doittask.api;

/**
 * Created by vivchik on 20.09.2018.
 */

import java.util.List;

import io.box.doittask.model.MessageForMock;
import retrofit2.Call;
import retrofit2.http.GET;

public interface HttpMock {
    @GET("messages1.json")
    Call<List<MessageForMock>> login();

    @GET("messages1.json")
    Call<List<MessageForMock>> register();

}