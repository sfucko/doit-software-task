package io.box.doittask.api;

import android.content.Context;

import java.util.List;

import io.box.doittask.AppController;
import io.box.doittask.model.BoxHelper;
import io.box.doittask.model.MessageForMock;
import io.box.doittask.model.User;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vivchik on 20.09.2018.
 */
//mock class that imitates http request

public class HttpRequests {
    private Context context;
    private Realm realm;

    public HttpRequests(Context context) {
        this.context = context;
        realm = Realm.getDefaultInstance();
    }

    //imitate registration on the server
    public void register(final SuccessfulListener successfulListener, final String name, final String email, final String password) {
        ((AppController) context.getApplicationContext()).getApi().register()
                .enqueue(new Callback<List<MessageForMock>>() {
                    @Override
                    public void onResponse(Call<List<MessageForMock>> call, Response<List<MessageForMock>> response) {
                        if (response.code() == 200) {
                            User user = new User(name, email.toLowerCase(), password, 1, BoxHelper.Small.RED.name(), false);
                            insertUser(user);
                            successfulListener.onSuccess();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<MessageForMock>> call, Throwable t) {
                        t.printStackTrace();
                        successfulListener.onFailure(t.toString());
                    }
                });
    }

    //imitate login on the server
    public void login(final SuccessfulListener successfulListener, String email, String password) {
        User user = realm.where(User.class).equalTo("email", email.toLowerCase()).findFirst();
        if (null == user)
            successfulListener.onFailure("User not found!");
        if (user.getPass().equals(password))
            successfulListener.onSuccess();
        else
            successfulListener.onFailure("Wrong password");
    }

    private void insertUser(User user) {
        realm.beginTransaction();
        Number maxId = realm.where(User.class).max("id");
        // If there are no rows, currentId is null, so the next id must be 1
        // If currentId is not null, increment it by 1
        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
        // User object created with the new Primary key
        user.setId(nextId);
        realm.copyToRealm(user);
        realm.commitTransaction();
    }

    public interface SuccessfulListener {
        public void onSuccess();

        public void onFailure(String message);
    }
}
