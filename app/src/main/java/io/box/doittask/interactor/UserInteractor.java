package io.box.doittask.interactor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import io.box.doittask.activity.ProfileActivity;
import io.box.doittask.api.HttpRequests;
import io.box.doittask.model.BoxHelper;
import io.box.doittask.model.User;
import io.box.doittask.model.ValidatorResponse;
import io.box.doittask.utils.Settings;
import io.box.doittask.utils.Validator;
import io.realm.Realm;

/**
 * Created by vivchik on 20.09.2018.
 */

public class UserInteractor {
    HttpRequests httpRequests;
    Validator validator;
    Realm realm;

    public UserInteractor() {
    }

    public UserInteractor(HttpRequests httpRequests, Validator validator) {
        this.httpRequests = httpRequests;
        this.validator = validator;
        realm = Realm.getDefaultInstance();

    }

    public void login(final Activity activity, final String email, String password) {
        ValidatorResponse validatorResponse = validator.isEmail(email);
        if (!validatorResponse.isSuccessful()) {
            Toast.makeText(activity, validatorResponse.getResultMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        httpRequests.login(new HttpRequests.SuccessfulListener() {

            @Override
            public void onSuccess() {
                Settings.setUsersEmail(activity, email);
                Intent intent = new Intent(activity, ProfileActivity.class);
                activity.startActivity(intent);
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

            }
        }, email, password);
    }

    public void register(final Activity activity, final String name, final String email,
                         final String password, String passwordConfirm) {
        ValidatorResponse validatorResponse = validator.isEmail(email);
        if (!validatorResponse.isSuccessful()) {
            Toast.makeText(activity, validatorResponse.getResultMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        validatorResponse = validator.isPasswordsMatch(password, passwordConfirm);
        if (!validatorResponse.isSuccessful()) {
            Toast.makeText(activity, validatorResponse.getResultMessage(), Toast.LENGTH_LONG).show();
            return;
        }
        validatorResponse = validator.isNewEmail(realm, email);
        if (!validatorResponse.isSuccessful()) {
            Toast.makeText(activity, validatorResponse.getResultMessage(), Toast.LENGTH_LONG).show();
            return;
        }

        httpRequests.register(new HttpRequests.SuccessfulListener() {

            @Override
            public void onSuccess() {
                Toast.makeText(activity, "Suceessfully registered!", Toast.LENGTH_SHORT).show();
                activity.finish();
            }

            @Override
            public void onFailure(String message) {
                Toast.makeText(activity, "Error! " + message, Toast.LENGTH_LONG).show();

            }
        }, name, email, password);
    }

    public void setColorList(Context context, int size, Spinner spinnerColor) {
        String[] colorsArray = BoxHelper.getColorsFromSize(size);
        // adapter for colors spinner
        ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, colorsArray);
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerColor.setAdapter(colorAdapter);
        colorAdapter.notifyDataSetChanged();
    }

    public void changeBox(User user, int size, String color) {
        realm.beginTransaction();
        user.setBoxColor(color);
        user.setBoxSize(size);

        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }
}
