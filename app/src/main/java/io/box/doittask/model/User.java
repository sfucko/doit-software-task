package io.box.doittask.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by vivchik on 18.09.2018.
 */

public class User extends RealmObject {
    @PrimaryKey
    private int id;

    private String name;
    private String email;
    private String pass;
    private int boxSize;
    private String boxColor;
    private boolean isNameNeeded;

    public User() {
    }

    public User(String name, String email, String pass, int boxSize, String boxColor, boolean isNameNeeded) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.pass = pass;
        this.boxSize = boxSize;
        this.boxColor = boxColor;
        this.isNameNeeded = isNameNeeded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getBoxSize() {
        return boxSize;
    }

    public void setBoxSize(int boxSize) {
        this.boxSize = boxSize;
    }

    public String getBoxColor() {
        return boxColor;
    }

    public void setBoxColor(String boxColor) {
        this.boxColor = boxColor;
    }

    public boolean isNameNeeded() {
        return isNameNeeded;
    }

    public void setNameNeeded(boolean nameNeeded) {
        isNameNeeded = nameNeeded;
    }
}
