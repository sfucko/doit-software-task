package io.box.doittask.model;

/**
 * Created by vivchik on 20.09.2018.
 */

public class ValidatorResponse {
    private boolean isSuccessful;
    private String resultMessage;

    public ValidatorResponse(boolean isSuccessful, String resultMessage) {
        this.isSuccessful = isSuccessful;
        this.resultMessage = resultMessage;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }
}
