package io.box.doittask.model;

import io.box.doittask.R;

/**
 * Created by vivchik on 19.09.2018.
 */

public class BoxHelper {
    public BoxHelper() {
    }

    public enum Small {
        RED(R.color.boxRed), BLUE(R.color.boxBlue), YELLOW(R.color.boxYellow);
        private int code;

        Small(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }

    public enum Medium {
        RED(R.color.boxRed), YELLOW(R.color.boxYellow), PURPLE(R.color.boxPurple), GREEN(R.color.boxGreen);
        private int code;

        Medium(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }

    public enum Large {
        ORANGE(R.color.boxOrange), GREEN(R.color.boxGreen), BLUE(R.color.boxBlue);
        private int code;

        Large(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }

    public static String[] getColorsFromSize(int size) {
        if (size == 1)
            return getSmallColors();
        if (size == 2)
            return getMediumColors();
        else
            return getLargeColors();
    }

    public static String[] getSmallColors() {
        String[] colorsList = new String[Small.values().length];
        for (int i = 0; i < Small.values().length; i++) {
            colorsList[i] = Small.values()[i].toString();
        }
        return colorsList;
    }

    public static String[] getMediumColors() {
        String[] colorsList = new String[Medium.values().length];
        for (int i = 0; i < Medium.values().length; i++) {
            colorsList[i] = Medium.values()[i].toString();
        }
        return colorsList;
    }

    public static String[] getLargeColors() {
        String[] colorsList = new String[Large.values().length];
        for (int i = 0; i < Large.values().length; i++) {
            colorsList[i] = Large.values()[i].toString();
        }
        return colorsList;
    }
}
