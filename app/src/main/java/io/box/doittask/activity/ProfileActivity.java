package io.box.doittask.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.box.doittask.AppController;
import io.box.doittask.R;
import io.box.doittask.interactor.UserInteractor;
import io.box.doittask.model.BoxHelper;
import io.box.doittask.model.User;
import io.box.doittask.utils.Settings;
import io.realm.Realm;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.spinnerSize)
    Spinner spinnerSize;
    @BindView(R.id.spinnerColor)
    Spinner spinnerColor;
    @BindView(R.id.logout)
    Button logout;
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.edit)
    Button edit;

    @BindView(R.id.newBoxLayout)
    LinearLayout newBoxLayout;
    @BindView(R.id.oldBoxLayout)
    LinearLayout oldBoxLayout;
    @BindView(R.id.profileTitle)
    TextView profileTitle;
    @BindView(R.id.currentSize)
    TextView currentSize;
    @BindView(R.id.currentColor)
    TextView currentColor;

    @Inject
    UserInteractor interactor;

    String[] sizes = {"15 cm x 15 cm x 15 cm", "100 cm x 25 cm x 25 cm", "75 cm x 75 cm x 75 cm"};
    private Realm realm;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((AppController) getApplicationContext()).provideComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);
        initSpinners(1);
        realm = Realm.getDefaultInstance();

        user = realm.where(User.class).equalTo("email", Settings.getUsersEmail(this)).findFirst();

        currentColor.setText(user.getBoxColor());
        setSizeTextView();
    }

    private void setSizeTextView() {
        if (user.getBoxSize() == 1)
            currentSize.setText("Small");
        else if (user.getBoxSize() == 2)
            currentSize.setText("Medium");
        else
            currentSize.setText("Large");
    }

    private void initSpinners(int size) {
        String[] colorsArray = BoxHelper.getColorsFromSize(size);
        //adapter for sizes spinner
        ArrayAdapter<String> sizeAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, sizes);

        // adapter for colors spinner
        ArrayAdapter<String> colorAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, colorsArray);

        sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerColor.setAdapter(colorAdapter);
        spinnerSize.setAdapter(sizeAdapter);

        AdapterView.OnItemSelectedListener itemSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Spinner spinner = (Spinner) parent;
                if (spinner.getId() == R.id.spinnerSize) {
                    setColorList(position + 1);
                } else if (spinner.getId() == R.id.spinnerColor) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinnerSize.setOnItemSelectedListener(itemSelectedListener);
        spinnerColor.setOnItemSelectedListener(itemSelectedListener);
    }

    private void setColorList(int size) {
        interactor.setColorList(this, size, spinnerColor);
    }

    private void changeEditingStatus(boolean isEditing) {
        if (isEditing) {
            newBoxLayout.setVisibility(View.VISIBLE);
            oldBoxLayout.setVisibility(View.GONE);
            save.setVisibility(View.VISIBLE);
            profileTitle.setText(R.string.new_box);
        } else {
            newBoxLayout.setVisibility(View.GONE);
            oldBoxLayout.setVisibility(View.VISIBLE);
            save.setVisibility(View.GONE);
            profileTitle.setText(R.string.profile);

            currentColor.setText(spinnerColor.getSelectedItem().toString());
            setSizeTextView();
        }
    }

    @OnClick(R.id.save)
    public void save(View view) {
        interactor.changeBox(user, spinnerSize.getSelectedItemPosition() + 1, spinnerColor.getSelectedItem().toString());
        changeEditingStatus(false);
    }

    @OnClick(R.id.logout)
    public void logout(View view) {
        finish();
    }

    @OnClick(R.id.edit)
    public void edit(View view) {
        changeEditingStatus(true);
    }

}
