package io.box.doittask.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.box.doittask.AppController;
import io.box.doittask.R;
import io.box.doittask.interactor.UserInteractor;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.nameEditText)
    EditText nameEditText;
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;
    @BindView(R.id.passwordConfirmEditText)
    EditText passwordConfirmEditText;

    @Inject
    public UserInteractor interactor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((AppController) getApplicationContext()).provideComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.register)
    public void register(View view) {
        String name = nameEditText.getText().toString().trim();
        String email = emailEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String passwordConfirm = passwordConfirmEditText.getText().toString().trim();

        interactor.register(this, name, email, password, passwordConfirm);
    }

    @OnClick(R.id.cancel)
    public void cancel(View view) {
        finish();
    }
}
