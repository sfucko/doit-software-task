package io.box.doittask.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.box.doittask.AppController;
import io.box.doittask.R;
import io.box.doittask.interactor.UserInteractor;
import io.box.doittask.model.User;
import io.box.doittask.utils.Settings;
import io.realm.Realm;
import io.realm.RealmResults;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.emailEditText)
    EditText emailEditText;
    @BindView(R.id.passwordEditText)
    EditText passwordEditText;

    @Inject
    public UserInteractor interactor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ((AppController) getApplicationContext()).provideComponent().inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        final RealmResults<User> users = Realm.getDefaultInstance().where(User.class).findAll();

        emailEditText.setText(Settings.getUsersEmail(this));
    }

    @OnClick(R.id.register)
    public void register(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.login)
    public void login(View view) {
        interactor.login(this, emailEditText.getText().toString().trim(),
                passwordEditText.getText().toString().trim());
    }
}
