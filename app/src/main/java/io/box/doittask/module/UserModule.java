package io.box.doittask.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.box.doittask.api.HttpRequests;
import io.box.doittask.interactor.UserInteractor;
import io.box.doittask.utils.Validator;

/**
 * Created by vivchik on 19.09.2018.
 */
@Module
public class UserModule {
    Context context;

    public UserModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public Validator provideValidator() {
        return new Validator();
    }

    @Provides
    public HttpRequests provideHttpRequests() {
        return new HttpRequests(context);
    }

    @Provides
    public UserInteractor provideInteractor(HttpRequests httpRequests, Validator validator) {
        return new UserInteractor(httpRequests, validator);
    }
}
