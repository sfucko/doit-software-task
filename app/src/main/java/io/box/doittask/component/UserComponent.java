package io.box.doittask.component;

import javax.inject.Singleton;

import dagger.Component;
import io.box.doittask.activity.LoginActivity;
import io.box.doittask.activity.ProfileActivity;
import io.box.doittask.activity.RegisterActivity;
import io.box.doittask.module.UserModule;

/**
 * Created by vivchik on 19.09.2018.
 */
@Singleton
@Component(modules = {UserModule.class})
public interface UserComponent {
    void inject(LoginActivity activity);

    void inject(RegisterActivity activity);

    void inject(ProfileActivity activity);
}
