package io.box.doittask.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by vivchik on 20.09.2018.
 */

public class Settings {

    // setup users email when login
    public static void setUsersEmail(Context context, String value) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        mPrefs.edit().putString("usersEmail", value).commit();
    }

    public static String getUsersEmail(Context context) {
        SharedPreferences mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return mPrefs.getString("usersEmail", "");
    }
}
