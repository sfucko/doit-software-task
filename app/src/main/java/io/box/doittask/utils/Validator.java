package io.box.doittask.utils;

import io.box.doittask.model.User;
import io.box.doittask.model.ValidatorResponse;
import io.realm.Realm;

/**
 * Created by vivchik on 20.09.2018.
 */

public class Validator {

    public ValidatorResponse isPasswordsMatch(String pass1, String pass2) {
        if (pass1.equals(pass2))
            return new ValidatorResponse(true, "");
        else
            return new ValidatorResponse(false, "Passwords do not match");
    }

    public ValidatorResponse isEmail(String email) {
        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
            return new ValidatorResponse(true, "");
        else
            return new ValidatorResponse(false, "Wrong email");
    }

    public ValidatorResponse isNewEmail(Realm realm, String email) {
        User user = realm.where(User.class).equalTo("email", email).findFirst();
        if (null == user)
            return new ValidatorResponse(true, "");
        else
            return new ValidatorResponse(false, "Email is already used");


    }
}
