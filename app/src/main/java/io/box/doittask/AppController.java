package io.box.doittask;

import android.app.Application;

import io.box.doittask.api.HttpMock;
import io.box.doittask.component.DaggerUserComponent;
import io.box.doittask.component.UserComponent;
import io.box.doittask.module.UserModule;
import io.realm.Realm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vivchik on 20.09.2018.
 */

public class AppController extends Application {
    private UserComponent component;
    private static HttpMock httpMock;
    private Retrofit retrofit;

    public UserComponent provideComponent() {
        if (component == null) {
            component = DaggerUserComponent.builder().userModule(new UserModule(getApplicationContext())).build();
        }
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());

        retrofit = new Retrofit.Builder()
                .baseUrl("https://rawgit.com/startandroid/data/master/messages/") //Base address
                .addConverterFactory(GsonConverterFactory.create()) //JSON converter
                .build();
        httpMock = retrofit.create(HttpMock.class); //Object for requests
    }

    public static HttpMock getApi() {
        return httpMock;
    }
}
